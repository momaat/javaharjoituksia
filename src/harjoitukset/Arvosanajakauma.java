package harjoitukset;

import java.util.Scanner;
public class Arvosanajakauma {

	public static void main(String[] args) {
		
		Scanner lukija = new Scanner(System.in);
		
		int i = 0;
		
		System.out.println("Sy�t� arvosanat (10 kpl)");
		
		int[] arvosanat = {0, 0, 0, 0, 0, 0};
		while(i < 10) {
			int arvosana = lukija.nextInt();
			
			
			if (arvosana == 0) {
				arvosanat[0] = arvosanat[0] + 1;
			} else if (arvosana == 1) {
				arvosanat[1] = arvosanat[1] + 1;
			} else if (arvosana == 2) {
				arvosanat[2] = arvosanat[2] + 1;
			} else if (arvosana == 3) {
				arvosanat[3] = arvosanat[3] + 1;
			} else if (arvosana == 4) {
				arvosanat[4] = arvosanat[4] + 1;
			} else if (arvosana == 5) {
				arvosanat[5] = arvosanat[5] + 1;
			}
			
			i++;
			
		}
		
		for (int j = 0; j < arvosanat.length; j++) {
			System.out.print(j + ": ");
			tulostaTahtia(arvosanat[j]);
			System.out.println();
		}
		
	}
	
	public static void tulostaTahtia(int luku) {
		for (int i = 0; i < luku; i++) {
			System.out.print("*");
		}
	}

}
