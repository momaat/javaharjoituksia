package harjoitukset;

public class Taulukoita {

	public static void main(String[] args) {
			
		int[] taulukko = {5, 7, 32, 31, 8};
		
		double[] desit = {12.4, -13.55, 67.44};
		
		String[] merkit = {"Helsinki", "Lissabon", "New York", "Shanghai"};
		
		System.out.println("Alkiossa numero 3 on: " + merkit[2]);
		System.out.println("Alkiossa numero 5 on: " + taulukko[4]);
		System.out.println("Desimaalitaulukossa on " + desit.length + " arvoa");
		
		
		for (int i : taulukko) {
			System.out.println(i);
		}
		
		for (int i = 0; i < desit.length; i++) {
			System.out.println(desit[(desit.length - 1)-i]);
		}
		
		for (String s : merkit) {
			System.out.println(s);
		}
		
		int i = 0;
		do {
			merkit[i] = "tyhj�";
			System.out.println(merkit[i]);
			i++;
		} while (i < merkit.length) ;
	
	}
}
