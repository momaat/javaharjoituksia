package harjoitukset;

public class Valuuttamuunnin {

	public static void main(String[] args) {
		System.out.println("***********************************");
		System.out.println("* Euroa        =        Markkaa   *");
		System.out.println("***********************************");
		
		double markka = 5.94573;
		char merkki = '=';
		for (int i = 1; i <= 10; i ++) {
			double kurssi = (i*markka);
			//f = float, d = Integer, s = String, b = boolean
			System.out.printf("%5d %10c %12.2f \n",i, merkki, kurssi);
		}
	}

}
