package harjoitukset;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner lukija = new Scanner(System.in);
		
		
		while(true) {
			
			System.out.println("Anna vuosiluku: ");
			int vuosi = Integer.valueOf(lukija.nextLine());
			
			if (vuosi == 1) {
				break;
			}
			
			if (vuosi >= 1753) {
				if  (vuosi % 4 == 0 && vuosi % 100 != 0) {
					System.out.println("On karkausvuosi");
				} else if (vuosi % 400 == 0) {
					System.out.println("On karkausvuosi");
				} else {
					System.out.println("Ei ole karkausvuosi!");
				}
				
			} else if (vuosi < 1753) {
				if (vuosi % 4 == 0) {
					System.out.println("On karkausvuosi");
				} else {
					System.out.println("Ei ole karkausvuosi");
				}
			}
		}

		
		System.out.println("Anna s�hk�postiosoite: ");
		String sposti = lukija.nextLine();
		
		int maara = 0;
		for(int i = 0; i < sposti.length(); i++) {
			if (sposti.charAt(i) == '@') {
				maara += 1;
			}
		}
		if (maara == 0) {
			System.out.println("Et sy�tt�nyt @-merkki�");
		} else if (maara > 1) {
			System.out.println("Sy�tit liikaa @-merkkej�");
		} else if (sposti.contains(".") && sposti.length() >= 5) {
			System.out.println("Osoite kelpaa");
		} else {
			System.out.println("Virheellinen osoite");
		}
		
		
		
	}
}
