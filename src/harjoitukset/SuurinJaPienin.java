package harjoitukset;

public class SuurinJaPienin {

	public static void main(String[] args) {
		
		int[] taulukko = new int[100];
		
		for (int i = 0; i < taulukko.length; i++) {
			
			int luku = (int) (Math.random()*1000);
			taulukko[i] = luku;
		}
		
		for (int i : taulukko) {
			System.out.print(i + ", ");
		}
		
		int suurin = taulukko[0];
		for (int i = 1; i < taulukko.length; i++) {
			if (suurin < taulukko[i]) {
				suurin = taulukko [i];
			}
		}
		
		System.out.println();
		System.out.println("Suurin luku on " + suurin);
		
		int pienin = taulukko[0];
		for (int i = 1; i < taulukko.length; i++) {
			if(pienin > taulukko[i]) {
				pienin = taulukko[i];
			}
		}

		System.out.println();
		System.out.println("Pienin luku on " + pienin);
	}

}
