package harjoitukset;

import java.util.Scanner;

public class Laivanupotus {

	public static void main(String[] args) {
		
		Scanner lukija = new Scanner(System.in);
		
		char[][] taulukko = new char[5][5];
		
		taulukko[0][1] = '*';
		taulukko[2][3] = '*';
		taulukko[2][4] = '*';
		taulukko[3][0] = '*';
		taulukko[4][2] = '*';
		
		int osumat = 0;
		
		while(osumat < 5) {
		
			System.out.println("Mihin ammutaan? (x-koodinaatti v�lill� 0-4, y-koordinaatti v�lill� 0-4)");
			String koordinaatti = lukija.nextLine();
			
			
			
			String[] palat = koordinaatti.split("");
			int x = Integer.valueOf(palat[0]);
			int y = Integer.valueOf(palat[1]);
			
			if (x > 4 || y > 4) {
				System.out.println("Koordinaatin pit�� olla v�lill� 0-4!");
				continue;
			} 
			
			if (taulukko[x][y] == '*') {
				System.out.println("Osuma!");
				osumat++;
				
			} else {
				System.out.println("Ohi meni! Sy�t� uudet koordinaatit.");
			}
					
			
		
		}
		
		System.out.println("Voitit!");
	}

}
