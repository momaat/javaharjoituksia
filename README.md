Java exercises 

These exercises were made during Code Bootcamp - a four week's intensive training organized by Saranen Consulting Oy.

This repository contains the following files:

Main.java <br>
Arvosanajakauma.java <br>
Laivanupotus.java <br>
SuurinJaPienin.java <br>
Taulukoita.java <br>
Valuuttamuunnin.java
